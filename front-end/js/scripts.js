$(document).ready(function(){

	// script para ativar automaticamente algumas coisas do bootstrap
	$('[data-toggle="tooltip"], .redes-sociais > a').tooltip({trigger: 'hover'});
	$('[data-toggle="popover"]').popover({trigger: 'hover'});

	$('#btn-mobile-menu').click(function() {
		var menu = $('#menu > ul');
		if (menu.hasClass('active')) {
			menu.slideUp(200, function() {
				$(menu).removeClass('active');
			});
		} else {
			menu.slideDown(200, function() {
				$(menu).addClass('active');
			});
		}
	});

	jQuery(function($){
		$('[data-mask]').each(function(index, el) {
		   var mascaraCampo = $(this).data('mask');
		   $(this).mask(mascaraCampo);
		});

		/**
		* @author Marlon
		* @since 26/07/2015
		*/
		$('input.mask-fone').focusout(function(){
			var phone, element;
			element = $(this);
			element.unmask();
			phone = element.val().replace(/\D/g, '');
			if(phone.length > 10) {
				element.mask("(99) 99999-999?9");
			} else {
				element.mask("(99) 9999-9999?9");
			}
		}).trigger('focusout');
	});

});